library ttt.game.ui;

abstract class UI {
  void getGameType(void callback(String));

  void getUserInput(void callback(String));

  void shouldRestartGame(void callback(String));
}
