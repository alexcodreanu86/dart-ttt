library ttt.players.player;

import 'package:ttt/game/ui.dart';
import 'package:ttt/game/stateChecker.dart';
import 'strategies/minimax.dart';
import 'human.dart';
import 'ai.dart';



abstract class Player {
  String symbol;
  static String HUMAN_PLAYER_TYPE = 'human';

  factory Player(
      String inSymbol,
      String playerType,
      UI inUi) {

    if (playerType == HUMAN_PLAYER_TYPE) {
      return new Human(inSymbol, new StateChecker(), inUi);
    } else {
      return new AI(inSymbol, new Minimax(inSymbol, new StateChecker()));
    }
  }

  void getMove(List<String> boardState, void callback(String, int, Error));
}