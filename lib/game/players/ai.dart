library ttt.game.players.ai;

import 'dart:async';
import 'package:ttt/game/stateChecker.dart';
import 'strategies/minimax.dart';
import 'player.dart';

class AI implements Player {
  String symbolX = 'X', symbolO = 'O';

  String symbol;
  StateChecker stateChecker;
  Minimax aiStrategy;
  AI(String inSymbol, Minimax inAiStrategy)
      : symbol = inSymbol, aiStrategy = inAiStrategy;

  void getMove(List<String> boardState, void callback(String, int, Error)){
    _getStrategyMove(boardState).then((move) =>
        callback(symbol, move, null));
  }

  Future<int> _getStrategyMove(List<String> boardState) =>
    new Future(() => aiStrategy.bestMoveForPlayer(boardState, symbol));
}
