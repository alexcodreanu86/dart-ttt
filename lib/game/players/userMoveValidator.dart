library ttt.game.players.userMoveValidator;

import 'package:ttt/game/stateChecker.dart';

class UserMoveValidator {
  RegExp singleDigitMatcher = new RegExp(r'^\d$');

  StateChecker stateChecker;
  UserMoveValidator(this.stateChecker);

  bool isValid(String input, board) {
    return _isProperFormat(input) && stateChecker.isAvailableMove(int.parse(input), board);
  }

  bool _isProperFormat(String input) {
    return input.contains(singleDigitMatcher);
  }
}