library ttt.players.difficulty.minimax;

import 'package:ttt/game/stateChecker.dart';

class Minimax {
  final String _symbolX = 'X', _symbolO = 'O';

  String _aiSymbol;
  StateChecker _stateChecker;

  Minimax(String aiSymbol, StateChecker stateChecker)
  : _aiSymbol = aiSymbol, _stateChecker = stateChecker;

  int bestMoveForPlayer(List<String> boardState,
                                 String currentPlayerSymbol, [int depth = 0]) {
    Map<int, int> movesWithScores = _stateMovesScores(boardState, currentPlayerSymbol, depth);
    if(currentPlayerSymbol == _aiSymbol) {
      return _moveWithMaxScore(movesWithScores);
    } else {
      return _moveWithMinScore(movesWithScores);
    }
  }

  Map<int, int> _stateMovesScores(List<String> boardState, String currentPlayerSymbol, int depth) {
    List<int> availableMoves = _stateChecker.availableMoves(boardState);
    Map<int, int> movesWithScores = <int, int>{};
    availableMoves.forEach((move) {
      List<String> temporaryState = new List.from(boardState);
      temporaryState[move] = currentPlayerSymbol;
      movesWithScores[move] = _stateScore(temporaryState, currentPlayerSymbol, depth);
    });
    return movesWithScores;
  }

  int _stateScore(List<String> boardState,String currentPlayerSymbol, int depth) {
    String winnerSymbol = _stateChecker.getWinner(boardState);
    if(winnerSymbol == _aiSymbol) {
      return 10 - depth;
    } else if (winnerSymbol == _otherPlayerSymbol(_aiSymbol)) {
      return depth - 10;
    } else if (_stateChecker.isBoardFull(boardState)) {
      return 0;
    } else {
      return _scoreNextRound(boardState, _otherPlayerSymbol(currentPlayerSymbol), depth + 1);
    }
  }

  int _scoreNextRound(List<String> boardState, String playerSymbol, int depth) {
    int playerMove = bestMoveForPlayer(boardState, playerSymbol, depth);
    boardState[playerMove] = playerSymbol;
    return _stateScore(boardState, playerSymbol, depth);
  }

  String _otherPlayerSymbol(String sym) {
    return sym == _symbolX ? _symbolO : _symbolX;
  }

  int _moveWithMaxScore(Map<int, int> movesWithScores) {
      return _moveWithScoreForComparer(movesWithScores,
              (bestMove, otherMove) => bestMove < otherMove);
    }

    int _moveWithMinScore(Map<int, int> movesWithScores) {
      return _moveWithScoreForComparer(movesWithScores,
          (bestMove, otherMove) => otherMove < bestMove);
    }

    int _moveWithScoreForComparer(Map<int, int> movesWithScores,
                                 Function compare) {
      int bestMove;
      movesWithScores.forEach((move, score) {
        if (bestMove == null || compare(movesWithScores[bestMove], movesWithScores[move])){
          bestMove = move;
        }
      });

      return bestMove;
    }

}
