library ttt.game.players.human;

import 'package:ttt/game/ui.dart';
import 'package:ttt/game/stateChecker.dart';
import 'userMoveValidator.dart';
import 'player.dart';
import 'package:ttt/game/invalidInputError.dart';

class Human implements Player {
  UI ui;
  String symbol;
  UserMoveValidator validator;

  Human(String inSymbol, StateChecker inStateChecker, UI inUI) {
    symbol = inSymbol;
    validator = new UserMoveValidator(inStateChecker);
    ui = inUI;
  }

  void getMove(List<String> boardState, void callback(String, int, Error))  {
    ui.getUserInput((choice) =>
      _processInput(boardState, choice, callback));
  }

  void _processInput(List<String> boardState, String input, void callback(String, int, Error)) {
    if (validator.isValid(input, boardState)) {
      callback(symbol, int.parse(input), null);
    } else {
      callback(symbol, null, new InvalidInputError());
    }
  }
}