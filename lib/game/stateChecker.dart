library ttt.game.stateChecker;

class StateChecker {
  final List<List<int>> WINNING_COMBOS = [
    [0,1,2], [3,4,5], [6,7,8],
    [0,3,6], [1,4,7], [2,5,8],
    [0,4,8], [2,4,6]
  ];

  List<int> availableMoves(List<String> board) {
    List<int> moves = [];
    for(int i = 0; i < board.length; i++) {
      if (isAvailableMove(i, board)) moves.add(i);
    }

    return moves;
  }

  String getWinner(List<String> board) {
    String winnerSymbol;
    for(List<int> combo in WINNING_COMBOS) {
      if (_isWinningCombo(board, combo)) {
        winnerSymbol = board[combo[0]];
        break;
      }
    };
    return winnerSymbol;
  }

  bool isAvailableMove(int move, List<String> board) {
    return move >= 0 && move < board.length && board[move] == null;
  }

  bool isBoardFull(List<String> board) {
    return !board.contains(null);
  }

  bool _isWinningCombo(List<String> board, List<int> combo) {
    return board[combo[0]] == board[combo[1]] &&
           board[combo[0]] == board[combo[2]] &&
           board[combo[0]] != null;
  }
}
