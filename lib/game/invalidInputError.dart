library ttt.game.invalidInputError;

class InvalidInputError extends Error {
  String message = "Invalid input!";
}