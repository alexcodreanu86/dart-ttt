library ttt.game.display;

abstract class Display {
  void displayBoard(List<String> board);
  void invalidInput();
  void promptGameType();
  void promptPlayerMove(String playerSymbol);
  void promptRestartGame();
  void showTieGameOver();
  void showWinner(String winnerSymbol);
}
