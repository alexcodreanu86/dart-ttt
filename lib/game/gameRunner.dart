library ttt.game.gameRunner;

import 'package:ttt/locales/locale.dart';
import 'ui.dart';
import 'display.dart';
import 'controller.dart';

class GameRunner {
  Locale textProvider;
  UI ui;
  Display display;
  Controller controller;
  final String RESTART_INPUT = 'restart';
  static final String DEFAULT_GAME_TYPE = "human-vs-ai";

  final List<String> GAME_TYPES = [DEFAULT_GAME_TYPE, "ai-vs-human", "human-vs-human"];

  GameRunner(this.ui, this.display);

  void start() {
    display.promptGameType();
    ui.getGameType(_startController);
  }

  void _startController(String input) {
    controller = new Controller(ui, display, _processGameType(input), _gameEnded);
    controller.start();
  }

  void _gameEnded(String winner) {
    if (winner == null) {
      display.showTieGameOver();
    } else {
      display.showWinner(winner);
    }

    display.promptRestartGame();
    ui.shouldRestartGame(_shouldStartNewGame);
  }

  String _processGameType(input) {
    if (GAME_TYPES.contains(input)) {
      return input;
    } else {
      return DEFAULT_GAME_TYPE;
    }
  }

  void _shouldStartNewGame(String input) {
    if (input == RESTART_INPUT) {
      start();
    }
  }
}