library ttt.game.controller;

import 'package:ttt/game/ui.dart';
import 'package:ttt/game/display.dart';
import 'package:ttt/game/players/player.dart';
import 'package:ttt/game/stateChecker.dart';
import 'package:ttt/game/invalidInputError.dart';

class Controller {
  List<String> board;
  Display display;
  Function gameOverCallback;
  String gameType;
  Player player1, player2;
  UI ui;
  StateChecker stateChecker;
  final RegExp TYPE_SPLITTER = new RegExp(r'-vs-');

  Controller(UI inUI, Display inDisplay, String inGameType, void callback(String)) {
    ui = inUI;
    display = inDisplay;
    gameType = inGameType;
    stateChecker = new StateChecker();
    gameOverCallback = callback;
    board = _newBoard();
  }

  void start() {
    display.displayBoard(board);
    _generatePlayers();
    _playerTurn(player1);
  }

  void _generatePlayers() {
    List<String> playerTypes = gameType.split(TYPE_SPLITTER);
    player1 = new Player('X', playerTypes[0], ui);
    player2 = new Player('O', playerTypes[1], ui);
  }

  List<String> _newBoard() {
    return new List<String>(9);
  }

  void _playerTurn(Player player) {
    display.promptPlayerMove(player.symbol);
    player.getMove(board, _checkPlayerMove);
  }

  void _checkPlayerMove(String playerSymbol, int playerMove, InvalidInputError error) {
    if (error == null) {
      _updateGameState(playerSymbol, playerMove);
    } else {
      display.invalidInput();
      _playerTurn(_currentPlayer(playerSymbol));
    }
  }

  void _updateGameState(String playerSymbol, int playerMove) {
    board[playerMove] = playerSymbol;
    display.displayBoard(board);
    _nextPlayerTurn(playerSymbol);
  }

  void _nextPlayerTurn(String playerSymbol) {
    String winnerSymbol = stateChecker.getWinner(board);
    if(winnerSymbol != null || stateChecker.isBoardFull(board)){
      gameOverCallback(winnerSymbol);
    } else {
      _playerTurn(_nextPlayer(playerSymbol));
    }
  }

  Player _nextPlayer(String currentPlayerSymbol) {
    return player1.symbol == currentPlayerSymbol ? player2 : player1;
  }

  Player _currentPlayer(String playerSymbol) {
    return player1.symbol == playerSymbol ? player1 : player2;
  }
}
