library ttt.browser.browserDisplay;

import 'dart:html';
import 'package:ttt/game/display.dart';
import 'package:ttt/locales/locale.dart';

class BrowserDisplay implements Display {
  final String MESSAGES_CONTAINER = '[data-id="messages"]',
               CELL_SELECTOR = '[data-class="cell"]',
               SYMBOL_SELECTOR = 'i',
               ID_ATTRIBUTE = 'data-id',
               X_PLAYER_TOKEN = 'X',
               O_PLAYER_TOKEN = 'O',
               O_SYMBOL_CLASS = 'fa fa-circle-o fa-5x',
               X_SYMBOL_CLASS = 'fa fa-times fa-5x',
               EMPTY_BOX_CLASS = 'fa fa-square-o fa-5x';

  Locale textProvider;

  BrowserDisplay(this.textProvider);

  void displayBoard(List<String> board) {
    List<Element> cells = querySelectorAll(CELL_SELECTOR);
    _syncCellsStatuses(cells, board);
  }

  void invalidInput() {
    _messagesContainerText(textProvider.invalidInput);
  }

  void promptGameType() {
    _messagesContainerText(textProvider.gameTypePrompt);
  }

  void promptPlayerMove(String playerSymbol) {
    _messagesContainerText(textProvider.playerMovePrompt + playerSymbol);
  }

  void promptRestartGame() {
    String currentText = querySelector(MESSAGES_CONTAINER).text;
    querySelector(MESSAGES_CONTAINER).text = currentText + '! ' + textProvider.restartGamePrompt;
  }

  void showTieGameOver() {
    _messagesContainerText(textProvider.tieGame);
  }

  void showWinner(String winnerSymbol) {
    _messagesContainerText(textProvider.winnerMessage + winnerSymbol);
  }

  String _getClassNameForSymbol(String playerSymbol) {
    if (playerSymbol == X_PLAYER_TOKEN) {
      return X_SYMBOL_CLASS;
    } else if (playerSymbol == O_PLAYER_TOKEN) {
      return O_SYMBOL_CLASS;
    } else {
      return EMPTY_BOX_CLASS;
    }
  }

  void _messagesContainerText(String text) {
    querySelector(MESSAGES_CONTAINER).text = text;
  }

  void _showSymbolForCell(Element cell, String symbol) {
    Element symbolSection = cell.querySelector(SYMBOL_SELECTOR);
    symbolSection.className = _getClassNameForSymbol(symbol);
  }

  void _syncCellsStatuses(List<Element> cells, List<String> board) {
    for(Element cell in cells) {
      String cellDataId = cell.getAttribute(ID_ATTRIBUTE);
      _showSymbolForCell(cell, board[int.parse(cellDataId)]);
    }
  }
}
