library ttt.game.browserUi;

import 'dart:html';
import 'package:ttt/game/ui.dart';

class BrowserUI implements UI {
  final String CELL_SELECTOR = '[data-class="cell"]',
               GAME_TYPE     = '[data-class="game-type"]',
               RESTART_BUTTON  = '[data-id="restart"]',
               DATA_ID_ATTRIBUTE = 'data-id',
               DISABLED_ATTRIBUTE = 'disabled',
               DISABLED_VALUE = 'disabled';

  void getGameType(void callback(String)) {
    _enableGameTypeButtons();
    _getDataIdWhenClickedOnce(GAME_TYPE, (dataId) {
      _disableGameTypeButtons();
      callback(dataId);
    });
  }

  void getUserInput(void callback(String)) {
    _getDataIdWhenClickedOnce(CELL_SELECTOR, (dataId) =>
        callback(dataId));
  }

  void shouldRestartGame(void callback(String)) {
    _enableRestartButton();
    _getDataIdWhenClickedOnce(RESTART_BUTTON, (dataId) {
      _disableRestartButton();
      callback(dataId);
    });
  }

  void _getDataIdWhenClickedOnce(String selectorName, void callback(String)) {
    List eventListeners = [];
    querySelectorAll(selectorName).forEach((Element el) {
      eventListeners.add(el.onClick.listen((event) {
        Element el = event.currentTarget;
        _cancelEventListeners(eventListeners);
        callback(el.getAttribute(DATA_ID_ATTRIBUTE));
      }));
    });
  }

  void _cancelEventListeners(List eventListeners) {
    eventListeners.forEach((l) => l.cancel());
    eventListeners.clear();
  }

  void _disableGameTypeButtons() =>
      _gameTypeButtons().forEach(_disableButton);

  void _disableRestartButton() =>
      _disableButton(_restartButton());

  void _disableButton(Element button) {
    button.setAttribute(DISABLED_ATTRIBUTE, DISABLED_VALUE);
  }

  void _enableGameTypeButtons() =>
    _gameTypeButtons().forEach(_enableButton);

  void _enableRestartButton() =>
      _enableButton(_restartButton());

  void _enableButton(Element button) {
    button.attributes.remove(DISABLED_ATTRIBUTE);
  }

  List<Element> _gameTypeButtons() =>
        querySelectorAll(GAME_TYPE);

  Element _restartButton() =>
      querySelector(RESTART_BUTTON);
}
