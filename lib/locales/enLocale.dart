library ttt.locales.enLocale;

import 'locale.dart';

class EnLocale implements Locale {
  String tieGame = "Game ended in a tie",
         gameTypePrompt = "Select a game type!",
         invalidInput = "Move not available, try again!",
         winnerMessage = "Game Over! The winner is ",
         restartGamePrompt = "Do you want to play again?",
         playerMovePrompt = "Your move, player ",
         promptToTypeRestart = "Type 'restart' to start a new game!";
}
