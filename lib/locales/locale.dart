library ttt.locales.locale;

import 'enLocale.dart';
import 'roLocale.dart';
import 'plLocale.dart';

abstract class Locale {
  factory Locale([String language]) {
    if(language == 'ro') {
      return new RoLocale();
    } else
    if (language == 'pl') {
      return new PlLocale();
    } else {
      return new EnLocale();
    }
  }
  
  String tieGame,
    gameTypePrompt,
    invalidInput,
    winnerMessage,
    restartGamePrompt,
    playerMovePrompt,
    promptToTypeRestart;
}