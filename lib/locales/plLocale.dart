library ttt.locales.plLocale;

import 'locale.dart';

class PlLocale implements Locale {
  String tieGame = "Gra zakończyła się remisem!",
         gameTypePrompt = "Wybierz rodzaj gry!",
         invalidInput = "Przesuń niedostępna , spróbuj ponownie!",
         winnerMessage = "Gra zakończyła! Zwycięzcą jest ",
         restartGamePrompt = "Chcesz zagrać jeszcze raz?",
         playerMovePrompt = "Twój ruch , gracz ",
         promptToTypeRestart = "Typ 'restart' , aby rozpocząć nową grę!";
}