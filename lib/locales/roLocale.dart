library ttt.locales.roLocale;

import 'locale.dart';

class RoLocale implements Locale {
  String tieGame = "Jocul s-a terminat la egalitate",
         gameTypePrompt = "Alege tipul de joc!",
         invalidInput = "Miscarea nu a fost valabila, mai incearca odata!",
         winnerMessage = "Jocul s-a terminat! Castigatorul este ",
         restartGamePrompt = "Vrei sa mai joci odata?",
         playerMovePrompt = "Miscarea jucatorului ",
         promptToTypeRestart = "Tasteaza 'restart' pentru a incepe un joc nou!";
}