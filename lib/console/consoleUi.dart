library ttt.console.consoleUi;

import 'package:ttt/game/ui.dart';
import 'inputStreamWrapper.dart';

class ConsoleUI implements UI{
  InputStreamWrapper inputStream;
  
  ConsoleUI(this.inputStream);

  void getUserInput(void callback(String)) =>
    inputStream.getInput(callback);

  void getGameType(void callback(String)) =>
    inputStream.getInput(callback);

  void shouldRestartGame(void callback(String)) =>
    inputStream.getInput(callback);
}
