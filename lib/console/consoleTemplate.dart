library ttt.console.template;

class Template {
  static String rowSeparator    = "\n_____\n";
  static String emptySlot       = " ";
  static String columnSeparator = "|";

  static String render(List<String> board) {
    String renderedBoard = "";
    for(int i=0; i< board.length;i++) {
      if (isEndOfLine(i)) {
          renderedBoard += rowSeparator;
      }

      if (board[i] != null) {
          renderedBoard += board[i];
      } else {
          renderedBoard += emptySlot;
      }

      if (needsDivider(i)) {
          renderedBoard += columnSeparator;
      }
    }
    return renderedBoard;
  }

  static String gameTypeMenu() {
    return "Options:\n human-vs-ai (default)\n ai-vs-human\n human-vs-human";
  }

  static bool isEndOfLine(int index) {
    return index != 0 && (index % 3) == 0;
  }

  static bool needsDivider(int index) {
      return (index % 3) != 2;
  }
}