library ttt.console.inputStreamWrapper;

class InputStreamWrapper {
  dynamic _inputStream;
  InputStreamWrapper(inputStream) {
    _inputStream = inputStream;
  }
  
  void getInput(void callback(String)) {
    callback(_inputStream.readLineSync());
  }
}