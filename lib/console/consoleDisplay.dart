library ttt.game.consoleDisplay;

import 'package:ttt/game/display.dart';
import 'consoleTemplate.dart';
import 'package:ttt/locales/locale.dart';

class ConsoleDisplay implements Display {
  var outputStream;
  Locale textProvider;
  final String NEW_LINE = "\n";

  ConsoleDisplay(argOutput, inTextProvider)
      : outputStream = argOutput, textProvider = inTextProvider;

  void displayBoard(List<String> board) {
    _show(Template.render(board));
  }

  void invalidInput() {
    _show(textProvider.invalidInput);
  }

  void promptGameType() {
    _show(textProvider.gameTypePrompt + NEW_LINE + Template.gameTypeMenu());
  }

  void promptPlayerMove(String playerSymbol) {
    _show(textProvider.playerMovePrompt + playerSymbol);
  }

  void promptRestartGame() {
    _show(textProvider.restartGamePrompt + NEW_LINE + textProvider.promptToTypeRestart);;
  }

  void showTieGameOver() {
    _show(textProvider.tieGame);
  }

  void showWinner(String winnerSymbol) {
    _show(textProvider.winnerMessage + winnerSymbol);
  }

  void _show(String text) {
    outputStream.writeln(text);
  }
}
