// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:ttt/game/gameRunner.dart';
import 'package:ttt/locales/locale.dart';
import 'package:ttt/browser/browserDisplay.dart';
import 'package:ttt/browser/browserUi.dart';

void main() {
  BrowserDisplay display = new BrowserDisplay(new Locale());
  BrowserUI ui = new BrowserUI();
  new GameRunner(ui, display).start();
}
