// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:io';
import 'package:ttt/game/gameRunner.dart';
import 'package:ttt/locales/locale.dart';
import 'package:ttt/console/consoleDisplay.dart';
import 'package:ttt/console/consoleUi.dart';
import 'package:ttt/console/inputStreamWrapper.dart';

main([List<String> args]) {
  Locale textProvider;
  if (args.length > 0) {
    textProvider = new Locale(args[0]);
  } else {
    textProvider = new Locale();
  }
  
  ConsoleDisplay display = new ConsoleDisplay(stdout, textProvider);
  ConsoleUI ui = new ConsoleUI(new InputStreamWrapper(stdin));
  new GameRunner(ui,display).start();
}