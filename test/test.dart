import 'package:unittest/html_enhanced_config.dart';

import 'browser/browserUi_test.dart' as BrowserUITest;
import 'browser/browserDisplay_test.dart' as BrowserDisplayTest;
import 'console/consoleUi_test.dart' as ConsoleUITest;
import 'console/consoleDisplay_test.dart' as ConsoleDisplayTest;
import 'console/consoleTemplate_test.dart' as ConsoleTemplateTest;
import 'console/inputStreamWrapper_test.dart' as ConsoleInputStreamWrapper;
import 'locales/locale_test.dart' as LocaleTest;
import 'game/gameRunner_test.dart' as GameRunnerTest;
import 'game/controller_test.dart' as ControllerTest;
import 'game/players/player_test.dart' as PlayerTest;
import 'game/players/human_test.dart' as HumanTest;
import 'game/players/ai_test.dart' as AITest;
import 'game/stateChecker_test.dart' as StateChecker;
import 'game/players/strategies/minimax_test.dart' as MinimaxTest;
import 'game/players/userMoveValidator_test.dart' as UserMoveValidatorTest;
import 'game/invalidInputError_test.dart' as InvalidInputErrorTest;
import 'fakes/fakeStdout_test.dart' as FakeStdoutTest;
import 'fakes/fakeInputStreamWrapper_test.dart' as FakeInputStreamWrapperTest;
import 'fakes/fakeStdin_test.dart' as FakeStdinTest;

main () {
  useHtmlEnhancedConfiguration();
  BrowserUITest.run();
  BrowserDisplayTest.run();
  ConsoleUITest.run();
  ConsoleDisplayTest.run();
  ConsoleTemplateTest.run();
  ConsoleInputStreamWrapper.run();
  LocaleTest.run();
  GameRunnerTest.run();
  ControllerTest.run();
  PlayerTest.run();
  HumanTest.run();
  AITest.run();
  StateChecker.run();
  MinimaxTest.run();
  UserMoveValidatorTest.run();
  InvalidInputErrorTest.run();
  FakeStdoutTest.run();
  FakeStdinTest.run();
  FakeInputStreamWrapperTest.run();
}