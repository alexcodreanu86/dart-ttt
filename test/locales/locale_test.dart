library ttt.locales.localeTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/locales/locale.dart';
import 'package:ttt/locales/enLocale.dart';
import 'package:ttt/locales/roLocale.dart';
import 'package:ttt/locales/plLocale.dart';

run() {
 group("Locale", () {
   group("Factory Constructor", () {
     test("returns EN locale when no argument is provided", (){
       expect(new Locale() is EnLocale, isTrue);
     });
     
     test("return RO locale when argument is 'ro'", () {
       expect(new Locale('ro') is RoLocale, isTrue);
     });
     
     test("returns PL locale when argument is 'pl'", () {
       expect(new Locale('pl') is PlLocale, isTrue);
     });
   });
 });
}
