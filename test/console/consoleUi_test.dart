library ttt.console.consoleUiTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/console/consoleUi.dart';
import '../fakes/fakeInputStreamWrapper.dart';

run() {
  group("ConsoleUI", () {
    FakeInputStreamWrapper inputStream;
    ConsoleUI ui;
    
    setUp(() {
      inputStream = new FakeInputStreamWrapper();
      ui = new ConsoleUI(inputStream);
    });

    test("getUserInput returns the input provided by the user", () {
      String userInput;
      ui.getUserInput((input) => userInput = input);
      inputStream.sendInput('2');
      expect(userInput, equals('2'));
    });

    test("getGameType calls the callback with the game state chosen by the user", () {
      String gameType;
      ui.getGameType((String input) => gameType = input);
      inputStream.sendInput('human-vs-human');
      
      expect(gameType, equals('human-vs-human'));
    });

    test("shouldRestartGame calls the callback with the given value", () {
      String response;
      ui.shouldRestartGame((String input) => response = input);
      inputStream.sendInput('no');
        
      expect(response, equals('no'));
    });
  });
}