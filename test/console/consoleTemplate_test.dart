library ttt.console.consoleTemplateTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/console/consoleTemplate.dart';

run() {
  group("ConsoleTemplate", () {
    test("render renders the current state of the board", () {
      List<String> board = ['X', 'O', 'X',
                            'O', null, 'X',
                           null, null, 'O'];
      String expectedResponse = "X|O|X\n_____\nO| |X\n_____\n | |O";

      expect(Template.render(board), equals(expectedResponse));
    });

    test("gameTypeMenu return the gameType menu", () {
      String expectedResponse = "Options:\n human-vs-ai (default)\n ai-vs-human\n human-vs-human";

      expect(Template.gameTypeMenu(), equals(expectedResponse));
    });
  });
}