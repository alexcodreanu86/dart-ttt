library ttt.console.consoleDisplay;

import 'package:unittest/unittest.dart';
import 'package:ttt/console/consoleDisplay.dart';
import 'package:ttt/console/consoleTemplate.dart';
import 'package:ttt/locales/enLocale.dart';
import '../fakes/fakeStdout.dart';

run() {
  group("ConsoleDisplay", () {
    ConsoleDisplay display;
    FakeStdout fakeStdout;
    EnLocale textProvider = new EnLocale();

    setUp(() {
      fakeStdout = new FakeStdout();
      display = new ConsoleDisplay(fakeStdout, textProvider);
    });

    test("promptGameType displays the game type prompt", () {
      display.promptGameType();

      expect(fakeStdout.lastOutput(), contains(textProvider.gameTypePrompt));
    });

    test("promptGameType displays the options menu", () {
      display.promptGameType();

      expect(fakeStdout.lastOutput(), contains(Template.gameTypeMenu()));
    });

    test("displayBoard displays the given board", () {
      List<String> board = ['X', 'O', 'X',
                            'O', null, 'X',
                           null, null, 'O'];
      String expectedOutput = "X|O|X\n_____\nO| |X\n_____\n | |O";
      display.displayBoard(board);

      expect(fakeStdout.lastOutput(), equals(expectedOutput));
    });

    test("showWinner displays the winner", () {
      display.showWinner('X');

      expect(fakeStdout.lastOutput(), equals(textProvider.winnerMessage + "X"));
    });

    test("showTieGameOver notifies the user that the game ended in a tie", () {
      display.showTieGameOver();

      expect(fakeStdout.lastOutput(), equals(textProvider.tieGame));
    });

    test("promptPlayerMove displays the userInputPrompt", () {
      display.promptPlayerMove('X');

      expect(fakeStdout.lastOutput(), equals(textProvider.playerMovePrompt + 'X'));
    });

    test("promptRestartGame displays the restartGamePrompt", () {
      display.promptRestartGame();

      expect(fakeStdout.lastOutput(), equals(textProvider.restartGamePrompt + "\n" + textProvider.promptToTypeRestart));
    });

    test("invalidInput displays the invalid input text", () {
      display.invalidInput();

      expect(fakeStdout.lastOutput(), equals(textProvider.invalidInput));
    });
  });
}
