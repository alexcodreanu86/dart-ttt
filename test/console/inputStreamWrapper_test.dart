library ttt.console.inputStreamWrapperTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/console/inputStreamWrapper.dart';
import '../fakes/fakeStdin.dart';

run() {
  group("InputStreamWrapper", () {
    test("getInput returns user the input returned by the stdin stream", () {
      FakeStdin fakeStdin = new FakeStdin(["input1", "input2"]);
      
      InputStreamWrapper wrapper = new InputStreamWrapper(fakeStdin);
      String input1;
      String input2;
      
      wrapper.getInput((String input) => input1 = input);
      wrapper.getInput((String input) => input2 = input);
      
      expect(input1, equals("input1"));
      expect(input2, equals("input2"));
    });
  });
}