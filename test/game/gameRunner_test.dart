library ttt.game.gameRunnerTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/gameRunner.dart';
import 'package:ttt/game/players/human.dart';
import 'package:ttt/game/players/ai.dart';
import 'package:ttt/game/controller.dart';
import 'package:ttt/locales/locale.dart';
import 'package:ttt/browser/browserDisplay.dart';
import 'package:ttt/browser/browserUi.dart';
import 'package:ttt/console/consoleUi.dart';
import 'package:ttt/console/consoleDisplay.dart';
import 'package:ttt/console/consoleTemplate.dart';
import 'dart:html';

import '../helpers.dart' as Helpers;
import '../fakes/fakeInputStreamWrapper.dart';
import '../fakes/fakeStdout.dart';

run() {
  String displayedMessage() {
    return querySelector('[data-id="messages"]').text;
  }

  void clickCellNumber(int cellId) {
    List<Element> cells = querySelectorAll('[data-class=cell]');
    cells[cellId].click();
  }

  void clickButton(String selector) {
    querySelector(selector).click();
  }
  
  group("GameRunner", () {
    Locale textProvider = new Locale();
    
    group("console", () {
      FakeStdout fakeStdout;
      FakeInputStreamWrapper inputStream;
      ConsoleUI ui;
      ConsoleDisplay display;
      GameRunner gameRunner;

      setUp(() {
        fakeStdout = new FakeStdout();
        display = new ConsoleDisplay(fakeStdout, textProvider);
        inputStream = new FakeInputStreamWrapper();
        ui = new ConsoleUI(inputStream);
        gameRunner = new GameRunner(ui, display);
      });

      test("start prompts the player to choose a game type", () {
        gameRunner.start();

        expect(fakeStdout.lastOutput(), contains(Template.gameTypeMenu()));
      });

      test("starts the controller with the appropriate players", () {
        gameRunner.start();

        inputStream.sendInput('human-vs-human');
        Controller controller = gameRunner.controller;

        expect(controller.player1.runtimeType, equals(Human));
        expect(controller.player2.runtimeType, equals(Human));
      });

      test("asks the player if they want to play one more game when controller finishes the game", () {
        gameRunner.start();

        inputStream.sendInput('human-vs-human');
        Controller controller = gameRunner.controller;
        controller.board = [null, 'X', 'X',
                            null, null, null,
                            null, null, null];

        inputStream.sendInput('0');
        expect(fakeStdout.lastOutput(), contains(textProvider.promptToTypeRestart));
      });

      test("asks the player to play a game in case of tie", () {
        gameRunner.start();
        inputStream.sendInput('human-vs-human');
        Controller controller = gameRunner.controller;
        controller.board = [null, 'O', 'X',
                             'X', 'O', 'O',
                             'O', 'X', 'X'];

        inputStream.sendInput('0');
        expect(fakeStdout.lastOutput(), contains(textProvider.promptToTypeRestart));
      });

      test("starts new Game when player input is restart", () {
        gameRunner.start();
        inputStream.sendInput('human-vs-human');
        Controller controller = gameRunner.controller;
        controller.board = [null, 'O', 'X',
                             'X', 'O', 'O',
                             'O', 'X', 'X'];

        inputStream.sendInput('0');
        inputStream.sendInput('restart');

        expect(fakeStdout.lastOutput(), contains(Template.gameTypeMenu()));
      });


      test("does not start new game if should restart is not restart", () {
        gameRunner.start();
        inputStream.sendInput('human-vs-human');
        Controller controller = gameRunner.controller;
        controller.board = [null, 'O', 'X',
                             'X', 'O', 'O',
                             'O', 'X', 'X'];
        
        inputStream.sendInput('0');
        inputStream.sendInput('not restart');

        expect(fakeStdout.lastOutput(), contains(textProvider.promptToTypeRestart));
      });

      test("defaults game type to human vs ai if different input is given", () {
        gameRunner.start();

        inputStream.sendInput('invalid game type');

        Controller controller = gameRunner.controller;

        expect(controller.player1.runtimeType, equals(Human));
        expect(controller.player2.runtimeType, equals(AI));
      });
    });
    
    group("browser", () {
      BrowserUI ui;
      BrowserDisplay display;
      GameRunner gameRunner;
      setUp(() {
        Helpers.renderBoard();
        ui = new BrowserUI();
        display = new BrowserDisplay(textProvider);
        gameRunner = new GameRunner(ui, display);
      });
      
      tearDown(() {
        Helpers.removeBoard();
      });
      test("start prompts the player to choose a game type", () {
        gameRunner.start();

        expect(displayedMessage(), equals(textProvider.gameTypePrompt));
      });
      
      test("starts the controller with the appropriate players", () {
        gameRunner.start();

        clickButton("[data-id='human-vs-human']");
        Controller controller = gameRunner.controller;

        expect(controller.player1.runtimeType, equals(Human));
        expect(controller.player2.runtimeType, equals(Human));
      });
      
      test("asks the player if they want to play one more game when controller finishes the game", () {
        gameRunner.start();

        clickButton("[data-id='human-vs-human']");
        Controller controller = gameRunner.controller;
        controller.board = [null, 'X', 'X',
                            null, null, null,
                            null, null, null];

        clickCellNumber(0);
        expect(displayedMessage(), equals(textProvider.winnerMessage + "X" + "! Do you want to play again?"));
      });
      
      test("asks the player to play a game in case of tie", () {
        gameRunner.start();

        clickButton("[data-id='human-vs-human']");
        Controller controller = gameRunner.controller;
        controller.board = [null, 'O', 'X',
                             'X', 'O', 'O',
                             'O', 'X', 'X'];

        clickCellNumber(0);
        expect(displayedMessage(), equals(textProvider.tieGame +'! ' + textProvider.restartGamePrompt));
      });
      
      test("starts new Game when player clicks restart", () {
        gameRunner.start();

        clickButton("[data-id='human-vs-human']");
        Controller controller = gameRunner.controller;
        controller.board = [null, 'O', 'X',
                             'X', 'O', 'O',
                             'O', 'X', 'X'];

        clickCellNumber(0);
        clickButton("[data-id='restart']");

        expect(displayedMessage(), equals(textProvider.gameTypePrompt));
      });
    });
  });
}
