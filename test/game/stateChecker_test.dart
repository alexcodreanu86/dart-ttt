library ttt.game.stateCheckerTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/stateChecker.dart';

run() {
  group("StateChecker", () {
    StateChecker  stateChecker;
    setUp(() {
      stateChecker = new StateChecker();
    });

    test("isAvailableMove returns true if the move is available", () {
      List<String> board = new List<String>(9);

      expect(stateChecker.isAvailableMove(3, board), isTrue);
    });

    test("isAvailableMove returns false if the move is already taken", () {
      List<String> board = new List<String>(9);
      board[5] = 'X';

      expect(stateChecker.isAvailableMove(5, board), isFalse);
    });

    test("isAvailableMove returns false if move is out of bounds", () {
      List<String> board = new List<String>(9);

      expect(stateChecker.isAvailableMove(9, board), isFalse);
    });

    test("availableMoves returns all moves for an empty board",() {
      List<int> availableMoves = stateChecker.availableMoves(new List<String>(9));
      expect(availableMoves.length, equals(9));
    });

    test("availableMoves return only the available moves", () {
      List<String> board = ['X', 'X', null,
                            'O', 'O', null,
                            null, null, 'X'];

      expect(stateChecker.availableMoves(board), equals([2,5,6,7]));
    });

    test("isBoardFull return true when board is full", () {
      List<String> board = ['O', 'X', 'O',
                            'X', 'O', 'X',
                            'X', 'O', 'X'];

      expect(stateChecker.isBoardFull(board), isTrue);
    });

    test("isBoardFull returns false when board is not full", () {
      List<String> board = ['O', 'X', 'O',
                            'X', 'O', 'X',
                            'X', 'O', null];

      expect(stateChecker.isBoardFull(board), isFalse);
    });

    test("getWinner return null when there is no winner", () {
      List<String> board = new List<String>(9);

      expect(stateChecker.getWinner(board), isNull);
    });

    test("getWinner returns 'X' when 'X' is the winner", () {
      List<String> board = ['X', 'X', 'O',
                            'X', 'O', 'X',
                            'X', 'O', null];

      expect(stateChecker.getWinner(board), equals('X'));
    });

    test("getWinner returns 'O' when 'O' is the winner", () {
      List<String> board = [null, 'O', 'O',
                             'X', 'O', 'X',
                             'X', 'O', null];

      expect(stateChecker.getWinner(board), equals('O'));
    });
  });
}
