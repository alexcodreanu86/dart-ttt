library ttt.game.players.playerTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/players/player.dart';
import 'package:ttt/game/players/human.dart';
import 'package:ttt/game/players/ai.dart';
import 'package:ttt/browser/browserUi.dart';

void run() {
  group("Player", () {
    group("FactoryConstructor", () {
      test("returns human player when playerType is human", () {
        BrowserUI ui = new BrowserUI();
        Player player = new Player('X', 'human', ui);

        expect(player.runtimeType, equals(Human));
      });

      test("return ai player when playeType is ai", () {
        BrowserUI ui = new BrowserUI();
        Player player = new Player('X', 'ai', ui);

        expect(player.runtimeType, equals(AI));
      });
    });
  });
}