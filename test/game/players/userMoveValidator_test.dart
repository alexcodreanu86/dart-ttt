library ttt.game.players.userMoveValidatorTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/players/userMoveValidator.dart';
import 'package:ttt/game/stateChecker.dart';

run() {
  group("UserMoveValidator", () {
    test("isValid returns true when input is valid", () {
      List<String> board = new List<String>(9);
      StateChecker stateChecker = new StateChecker();
      UserMoveValidator validator = new UserMoveValidator(stateChecker);

      expect(validator.isValid('0', board), isTrue);
    });

    test("isValid return false when move is not available", () {
      List<String> board = new List<String>(9);
      board[0] = 'X';
      StateChecker stateChecker = new StateChecker();
      UserMoveValidator validator = new UserMoveValidator(stateChecker);

      expect(validator.isValid('0', board), isFalse);
    });

    test("is valid return false when move is invalid format", () {
      List<String> board = new List<String>(9);
      StateChecker stateChecker = new StateChecker();
      UserMoveValidator validator = new UserMoveValidator(stateChecker);

      expect(validator.isValid('hello world3', board), isFalse);
    });
  });
}