library ttt.game.players.humanTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/players/human.dart';
import 'package:ttt/game/stateChecker.dart';
import 'package:ttt/console/consoleUi.dart';
import '../../fakes/fakeStdout.dart';
import '../../fakes/fakeInputStreamWrapper.dart';

void run() {
  group("Human", () {
    List<String> emptyBoard() {
      return new List<String>(9);
    }

    ConsoleUI ui;
    Human player;
    List<String> boardState;
    FakeStdout fakeStdout;
    FakeInputStreamWrapper inputStream;

    setUp(() {
      inputStream = new FakeInputStreamWrapper();
      ui = new ConsoleUI(inputStream);
      boardState = emptyBoard();
      fakeStdout = new FakeStdout();
      player = new Human('X', new StateChecker(), ui);
    });

    test("returns the move that is input through the ui with it's symbol", () {
      int move; String symbol; Error err;

      player.getMove(boardState, (s,m, e) {
        symbol = s;
        move = m;
        err = e;
      });
      inputStream.sendInput('3');

      expect(move, equals(3));
      expect(symbol, equals('X'));
      expect(err, isNull);
    });

    test("returns an error when move is not available", () {
      int move; Error err;
      boardState[7] = 'X';
      player.getMove(boardState, (_,m, e) {
        move = m;
        err = e;
      });
      inputStream.sendInput('7');

      expect(move, isNull);
      expect(err == null, isFalse);
    });

    test("returns an error when input is invalid", () {
      player.getMove(boardState, (_, __, e) {
        expect(e == null, isFalse);
      });
      inputStream.sendInput('Hello world');
    });
  });
}
