library ttt.players.aiTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/players/ai.dart';
import 'package:ttt/game/stateChecker.dart';
import 'package:ttt/game/players/strategies/minimax.dart';

run() {
  group("AI", () {
    test("symbol returns the player symbol", () {
      AI ai = new AI('O', new Minimax('O', new StateChecker()));

      expect(ai.symbol, equals('O'));
    });

    test("getMove returns a valid move", () {
      List<String> board = ['X', 'O' ,'X',
                            'O', null, null,
                            null, null, null];
      AI ai = new AI('O', new Minimax('O', new StateChecker()));

      ai.getMove(board,  (symbol, move, err) {
        expect(symbol, equals('O'));
        expect(move > 3 && move < 9, isTrue);
        expect(err, isNull);
      });
    });
  });
}
