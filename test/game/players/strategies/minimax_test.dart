library ttt.game.players.strategies.minimaxTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/players/strategies/minimax.dart';
import 'package:ttt/game/stateChecker.dart';

run() {
  group("Minimax", () {
    Minimax minimax;
    setUp((){
      minimax = new Minimax('O', new StateChecker());
    });

    test("bestMoveForPlayer returns the only move on the board", () {
      List<String> board = ['O', 'X', 'X',
                            'X', 'X', 'O',
                          null, 'O', 'X'];

      int move = minimax.bestMoveForPlayer(board, 'O');
      expect(move, equals(6));
    });

    test("bestMoveForPlayer prevents opponent from winning", () {
      List<String> board = new List<String>(9);
      board[0] = 'X';
      board[1] = 'X';
      board[3] = 'O';

      int move = minimax.bestMoveForPlayer(board, 'O');
      expect(move, equals(2));
    });

    test("bestMoveForPlayer takes the winning move when it is available", () {
      List<String> board = ['O', null, 'O',
                            'X', 'X', null,
                            null, null, null];

      int move = minimax.bestMoveForPlayer(board, 'O');
      expect(move, equals(1));
    });

    test("bestMoveForPlayer creates a fork when it is available", () {
      List<String> board = ['O', 'X', null,
                           null, 'X', null,
                           null, 'O', null];

      int move = minimax.bestMoveForPlayer(board, 'O');
      expect(move, equals(6));
    });

    test("bestMoveForPlayer return the move with the fastest win", () {
      List<String> board = ['X', null, null,
                            'X', 'O', null,
                            'O', null, 'X'];

      int move = minimax.bestMoveForPlayer(board, 'O');
      expect(move, equals(2));
    });
  });
}
