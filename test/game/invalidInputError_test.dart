library ttt.game.invalidInputErrorTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/invalidInputError.dart';

run () {
  group("InvalidInputError", () {
    test("is an error", () {
      try {
        throw new InvalidInputError();
        fail("Error not thrown");
      } on InvalidInputError catch (e){
        expect(e.message, equals("Invalid input!"));
      }
    });
  });
}