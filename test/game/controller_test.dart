library ttt.game.controllerTest;

import 'package:unittest/unittest.dart';
import 'package:ttt/game/controller.dart';
import 'package:ttt/console/consoleDisplay.dart';
import 'package:ttt/console/consoleUi.dart';
import '../fakes/fakeInputStreamWrapper.dart';
import 'package:ttt/locales/locale.dart';
import '../fakes/fakeStdout.dart';
import 'package:ttt/game/players/human.dart';
import 'package:ttt/game/players/ai.dart';
import 'package:ttt/console/consoleTemplate.dart';

void main() => run();

void run() {
  group("Controller", () {
    ConsoleUI ui;
    FakeStdout fakeStdout;
    FakeInputStreamWrapper inputStream;
    Controller controller;
    Locale textProvider = new Locale();
    ConsoleDisplay display;

    Controller newController(String gameType, Function callback) {
      return new Controller(ui, display, gameType, callback);
    }

    setUp(() {
      inputStream = new FakeInputStreamWrapper();
      ui = new ConsoleUI(inputStream);
      fakeStdout = new FakeStdout();
      display = new ConsoleDisplay(fakeStdout, textProvider);
    });

    test("start renders the board", () {
      controller = newController('human-vs-human', (_) {});
      controller.start();

      expect(fakeStdout.container[0], equals(Template.render(controller.board)));
    });

    test("start generates players for the game type selection", () {
      controller = newController('human-vs-human', (_) {});
      controller.start();

      expect(controller.player1.runtimeType, equals(Human));
      expect(controller.player2.runtimeType, equals(Human));
    });

    test("setting up game type generates players human-vs-human", () {
      controller = newController('human-vs-human', (_) {});
      controller.start();

      expect(controller.player1.runtimeType, equals(Human));
      expect(controller.player2.runtimeType, equals(Human));
    });

    test("setting up game type generates players human-vs-ai", () {
      controller = newController('human-vs-ai', (_) {});
      controller.start();

      expect(controller.player1.runtimeType, equals(Human));
      expect(controller.player2.runtimeType, equals(AI));
    });

    test("setting up game type generates players ai-vs-human", () {
      controller = newController('ai-vs-human', (_) {});
      controller.start();

      expect(controller.player1.runtimeType, equals(AI));
      expect(controller.player2.runtimeType, equals(Human));
    });

    test("game loop displays the player move", () {
      controller = newController('human-vs-human', (_) {});
      controller.start();
      inputStream.sendInput('0');

      expect(controller.board[0], equals('X'));
    });

    test("game loop prompts for next player move when game is not over", () {
      controller = newController('human-vs-human', (_) {});
      controller.start();

      expect(fakeStdout.lastOutput(), equals(textProvider.playerMovePrompt + 'X'));

      inputStream.sendInput('6');
      expect(fakeStdout.lastOutput(), equals(textProvider.playerMovePrompt + 'O'));
      inputStream.sendInput('3');

      expect(controller.board[6], equals('X'));
      expect(controller.board[3], equals('O'));
    });

    test("game loop notifies that the game ended in a draw", () {
      String winner;
      controller = newController('human-vs-human',
          (endOfGame) => winner = endOfGame);
      controller.board = ['O', 'X', 'O',
                           'X', 'O', 'X',
                           'X', 'O', null];
      controller.start();
      inputStream.sendInput('8');

      expect(winner, isNull);
    });

    test("game loop notifies user of invalid input and prompts them for input again", () {
      controller = newController('human-vs-human', (_) {});
      controller.start();

      expect(fakeStdout.lastOutput(), equals(textProvider.playerMovePrompt + 'X'));

      inputStream.sendInput('6');
      expect(fakeStdout.lastOutput(), equals(textProvider.playerMovePrompt + 'O'));
      inputStream.sendInput('6');

      expect(fakeStdout.container, contains(textProvider.invalidInput));
      expect(fakeStdout.lastOutput(), equals(textProvider.playerMovePrompt + 'O'));
      expect(controller.board[6], equals('X'));
    });

    test("game loop notifies that X won", () {
      String winner;
      controller = newController('human-vs-human',
          (endOfGame) => winner = endOfGame);

      controller.board = [ null, 'X', 'O',
                            'X', 'O', null,
                            'X', 'O', null];
      controller.start();
      inputStream.sendInput('0');

      expect(winner, equals('X'));
    });

    test("game loop notifies that O won", () {
      String winner;
      controller = newController('human-vs-human',
          (endOfGame) => winner = endOfGame);

      controller.board = [ null, null, 'O',
                            'X', 'O', null,
                            'X', 'O', null];
      controller.start();
      inputStream.sendInput('8');
      inputStream.sendInput('1');

      expect(winner, equals('O'));
    });
  });
}
