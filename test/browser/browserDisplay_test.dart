library ttt.browser.browserDisplayTest;

import 'dart:html';
import 'package:ttt/browser/browserDisplay.dart';
import 'package:unittest/unittest.dart';
import 'package:ttt/locales/enLocale.dart';
import '../helpers.dart' as Helpers;

run() {
  group("BrowserDisplay", () {

    String messagesBoxText() {
      return querySelector('[data-id="messages"]').text;
    }

    BrowserDisplay display;
    EnLocale textProvider = new EnLocale();
    setUp(() {
      Helpers.renderBoard();
      display = new BrowserDisplay(textProvider);
    });

    tearDown(() {
      Helpers.removeBoard();
    });

    test("displayBoard updates the board state", () {
      List<String> board = new List<String>(9);
      board[1] = 'X';
      board[3] = 'O';
      display.displayBoard(board);

      Element xMoveEl = querySelector('[data-id="1"]').firstChild;
      Element oMoveEl = querySelector('[data-id="3"]').firstChild;
      Element emptyCell = querySelector('[data-id="4"]').firstChild;
      String xMoveClassName = xMoveEl.className;
      String oMoveClassName = oMoveEl.className;
      String emptyCellClassName = emptyCell.className;


      expect(xMoveClassName, contains('fa-times'));
      expect(oMoveClassName, contains('fa-circle-o'));
      expect(emptyCellClassName, contains('fa-square-o'));
    });

    test("displays the given winner", () {
      display.showWinner('X');

      expect(messagesBoxText(), equals(textProvider.winnerMessage + 'X'));
    });

    test("showTieGameOver displays game ended in a tie", () {
      display.showTieGameOver();

      expect(messagesBoxText(), equals(textProvider.tieGame));
    });

    test("promptGameType prompts the user to choose game type", () {
      display.promptGameType();

      expect(messagesBoxText(), equals(textProvider.gameTypePrompt));
    });

    test("promptRestartGame adds the prompt to the existing message", () {
      display.showWinner('X');
      display.promptRestartGame();
      String expectedText = textProvider.winnerMessage + 'X! ' + textProvider.restartGamePrompt;

      expect(messagesBoxText(), equals(expectedText));
    });

    test("promptPlayerMove displays the playerMove prompt with the given symbol", () {
      display.promptPlayerMove('Y');

      expect(messagesBoxText(), equals(textProvider.playerMovePrompt + 'Y'));
    });

    test("invalidInput displays the invalid input text", () {
      display.invalidInput();

      expect(messagesBoxText(), equals(textProvider.invalidInput));
    });
  });
}
