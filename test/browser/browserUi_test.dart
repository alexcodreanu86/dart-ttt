library ttt.browser.browserUiTest;

import 'dart:html';
import 'package:ttt/browser/browserUi.dart';
import 'package:unittest/unittest.dart';
import '../helpers.dart' as Helpers;

run() {
  BrowserUI ui;
  group("BrowserUI", () {
    setUp(() {
      Helpers.renderBoard();
      ui = new BrowserUI();
    });

    tearDown(() {
      Helpers.removeBoard();
    });

    for(int i = 0; i < 9; i++) {
      test("getUserInput passes $i when cell [data-id=$i] is clicked as the argument of the callback", (){
        String clickedElement;
        ui.getUserInput((n) => clickedElement = n );

        clickCellNumber(i);

        expect(clickedElement, equals("$i"));
      });
    }

    test("getUserInput calls the callback only once", () {
      int count = 0;
      ui.getUserInput((_) => count++);

      clickCellNumber(2);
      clickCellNumber(4);

      expect(count, equals(1));
    });

    List<String> gameTypes = ['human-vs-human', 'human-vs-ai', 'ai-vs-human'];

    gameTypes.forEach((gameType) {
      test("getGameType passes '$gameType' when the $gameType button is clicked", (){
        assertReturnsGameType(ui, gameType);
      });
    });

    test("getGameType disables the buttons after the first click", () {
      int count = 0;
      ui.getGameType((t) => count++);

      querySelector('[data-id="human-vs-human"]').click();
      querySelector('[data-id="ai-vs-human"]').click();

      expect(count, equals(1));
    });

    test("shouldRestartGame calls the callback when restart button is clicked with the restart value", () {
      String value;
      ui.shouldRestartGame((String val) => value = val);

      expect(value, isNull);

      querySelector('[data-id="restart"]').click();
      expect(value, equals("restart"));
    });

    test("shouldRestartGame enables the restart button", () {
      String disabledValue = querySelector('[data-id="restart"]').attributes['disabled'];

      expect(disabledValue, equals('disabled'));

      ui.shouldRestartGame((_) {});
      String newValue = querySelector('[data-id="restart"]').attributes['disabled'];
      expect(newValue, isNull);
    });

    test("shouldRestartGame disables the button after it is clicked", () {
      ui.shouldRestartGame((_){});

      String disabledValue = querySelector('[data-id="restart"]').attributes['disabled'];
      expect(disabledValue, isNull);

      querySelector('[data-id="restart"]').click();

      String newValue = querySelector('[data-id="restart"]').attributes['disabled'];
      expect(newValue, equals('disabled'));
    });

    test("shouldRestartGame calls the callback only once", () {
      int count = 0;
      ui.shouldRestartGame((_) { count++;});

      querySelector('[data-id="restart"]').click();
      querySelector('[data-id="restart"]').click();

      expect(count, equals(1));
    });
  });
}

void assertReturnsGameType(BrowserUI ui, String type) {
  String gameType;
  ui.getGameType((t) => gameType = t);

  querySelector('[data-id=$type]').click();

  expect(gameType, equals(type));
}

void clickCellNumber(int cellId) {
  List<Element> cells = querySelectorAll('[data-class=cell]');
  cells[cellId].click();
}
