library ttt.testHelpers;

import 'dart:html';

String boardTemplate() {
  return
    '<div data-id="sandbox">' +
        '<h2 data-id="messages"></h2>' +
        '<button data-id="human-vs-human" data-class="game-type" disabled="disabled"></button>' +
        '<button data-id="ai-vs-human" data-class="game-type" disabled="disabled"></button>' +
        '<button data-id="human-vs-ai" data-class="game-type" disabled="disabled"></button>' +
        '<button data-id="restart" disabled="disabled">Restart</button>' +
        '<div data-id="0" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="1" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="2" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="3" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="4" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="5" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="6" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="7" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
        '<div data-id="8" data-class="cell"><i class="fa fa-square-o fa-5x"></i></div>' +
    '</div>';
}

void renderBoard() {
  Element el = new Element.html(boardTemplate(), validator: _htmlValidator);
  querySelector('body').append(el);
}

void removeBoard() {
  querySelector('[data-id=sandbox]').remove();
}

final NodeValidatorBuilder _htmlValidator = new NodeValidatorBuilder.common()
  ..allowElement('div', attributes: ['data-id', 'data-class'])
  ..allowElement('button', attributes: ['data-id', 'data-class'])
  ..allowElement('h2', attributes: ['data-id']);
