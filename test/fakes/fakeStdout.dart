library ttt.fakes.fakeStdout;

class FakeStdout {
  List<String> container = [];

  void writeln(String output) {
    container.add(output);
  }

  String lastOutput() {
    return container.last;
  }
}