library ttt.fakes.fakeStdoutTest;

import 'package:unittest/unittest.dart';
import 'fakeStdout.dart';

run() {
  group("FakeStdout", () {
    test("writeln adds arguments to output container", () {
      FakeStdout fakeStdout = new FakeStdout();
      fakeStdout.writeln("first output");
      fakeStdout.writeln("second output");

      expect(fakeStdout.container[0], equals("first output"));
      expect(fakeStdout.container[1], equals("second output"));
    });

    test("lastOuput return the last thing thing that was given to it", () {
      FakeStdout fakeStdout = new FakeStdout();

      fakeStdout.writeln("first output");
      expect(fakeStdout.lastOutput(), equals("first output"));

      fakeStdout.writeln("second output");
      expect(fakeStdout.lastOutput(), equals("second output"));
    });
  });
}