library ttt.stubs.stubStdin;

class FakeStdin {
  List<String> inputs;

  FakeStdin(this.inputs);

  String readLineSync() {
    return inputs.removeAt(0);
  }
}