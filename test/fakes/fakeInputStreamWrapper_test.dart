library ttt.fakes.fakeInputStreamWrapperTest;

import 'package:unittest/unittest.dart';
import 'fakeInputStreamWrapper.dart';
import 'package:ttt/console/inputStreamWrapper.dart';

run() {
  group("FakeInputStreamWrapperTest", () {
    test("calls the callback with the input given", () {
      FakeInputStreamWrapper wrapper = new FakeInputStreamWrapper();
      String fakeInput;
      wrapper.getInput((String input) => fakeInput = input);
      
      wrapper.sendInput("test");
      expect(fakeInput, equals("test"));
    });
    
    test("implements InputStreamWrapper", () {
      FakeInputStreamWrapper wrapper = new FakeInputStreamWrapper();
      expect(wrapper is InputStreamWrapper, isTrue);
    });
  });
}