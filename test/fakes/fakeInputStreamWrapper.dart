library ttt.fakes.fakeInputStreamWrapper;

import 'package:ttt/console/inputStreamWrapper.dart';

class FakeInputStreamWrapper implements InputStreamWrapper {
  Function inputCallback;
  @override
  void getInput(callback) {
    inputCallback = callback;
  }
  
  void sendInput(String fakeInput) {
    inputCallback(fakeInput);
  }
}