library ttt.fakes.fakeStdinTest;

import 'package:unittest/unittest.dart';
import 'fakeStdin.dart';

run() {
  group("FakeStdin", () {
    test("readLineSync return takes the value of the nextInput", () {
      FakeStdin fakeStdin = new FakeStdin(['1', '2', '3']);

      expect(fakeStdin.readLineSync(), equals('1'));
      expect(fakeStdin.readLineSync(), equals('2'));
      expect(fakeStdin.readLineSync(), equals('3'));
    });
  });
}